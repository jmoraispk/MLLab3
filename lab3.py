
#%%
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import tensorflow.keras as keras #avoids some problems with TF funcs...
from keras import utils, models, layers, callbacks, optimizers


from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score


x = np.load("mnist_train_data.npy")
y = np.load("mnist_train_labels.npy")

x_test = np.load("mnist_test_data.npy")
y_test = np.load("mnist_test_labels.npy")


print(x.shape)
print(y.shape)

#plt.imshow(np.squeeze(x[5]))

#normalizing
x_norm = x / 255
x_test_norm = x_test / 255

#transform y into a matrix where each column has a 
# 1 in that entrance.
labels = keras.utils.to_categorical(y, num_classes=None)

#%% First Model
model = keras.models.Sequential()

#A flatten layer to sequentialize the input: height x width x depth
# output will be: (row after row... 1D with 28x28x1 length)
#input shape of one sample
model.add(keras.layers.Flatten(input_shape = (28,28,1)))
#in case the samples are one dimensional, Input layer is enough.

#after the first layer it is not necessary to specify the input size
model.add(keras.layers.Dense(64, activation='relu'))
model.add(keras.layers.Dense(128, activation='relu'))
model.add(keras.layers.Dense(10, activation='softmax'))


#Expected: Layer 1 weights: (784+1) * 64 = 50240
#          Layer 2 weights: (64 +1) * 128 = 8320
#          Layer 3 weights: (128+1) *  10 = 1290
#                                Total   = 59850
model.summary()

stopper = keras.callbacks.EarlyStopping(patience=15, restore_best_weights=True)
#restore_best_weights - restore weights from best epoch 

# min_delta maybe should be different from 0...but seems to work..

Adam = keras.optimizers.Adam(lr=0.01, clipnorm = 1)

model.compile(loss='categorical_crossentropy', optimizer=Adam)

hist1 = \
    model.fit(x = x_norm, y=labels, batch_size=300, epochs=400,\
        verbose = 0, callbacks = [stopper], validation_split=0.3)
#shuffle??
#whether to shuffle the training data before each epoch


y_pred_labels = model.predict(x_test_norm)
        #batch_size=None, verbose=0, steps=None, callbacks=None, \
        #max_queue_size=10, workers=1, use_multiprocessing=False)

y_pred = np.argmax(y_pred_labels, axis=1)



#%% Plot training & validation loss values

#plt.style.use('default') #check available styles in matplotlib.style.available
#plt.style.use('dark_background') #default for vscode darktheme
matplotlib.style.use('default')
plt.plot(hist1.history['loss'])
plt.plot(hist1.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')

x_min = np.argmin(hist1.history['val_loss'])
y_min = round(hist1.history['val_loss'][x_min],3)

plt.text(x_min*0.90, y_min*1.20,'({}, {})'.format(x_min,y_min), fontsize=12)
plt.scatter(x_min, y_min, color='orange')

x_last = len(hist1.history['val_loss'])-1
y_last = round(hist1.history['val_loss'][x_last],3)

plt.text(x_last*0.90, y_last*1.20,'({}, {})'.format(x_last,y_last), fontsize=12)
plt.scatter(x_last, y_last, color='orange')

plt.savefig('lossVSval.png')
plt.show()





#%%

#having our model calculated, time to evaluate it with predict.
#predict generates outputs of each input.

y_pred_labels = model.predict(x_test_norm)
        #batch_size=None, verbose=0, steps=None, callbacks=None, \
        #max_queue_size=10, workers=1, use_multiprocessing=False)


y_pred = np.argmax(y_pred_labels, axis=1)

matrix = confusion_matrix(y_test, y_pred)

sns.heatmap(matrix,annot=True,cbar=True)

plt.ylabel('True Label')
plt.xlabel('Predicted Label')
plt.title('Confusion Matrix')

#print(accuracy_score(y_test, y_pred, normalize=False) / len(y_test))
print('Accuracy:', accuracy_score(y_test, y_pred, normalize=True)* 100, '%')

#%% Normalized confusion matrix

#Note that if the test data is not equally distributed between labels, the 
#confusion matrix on percentages can be misleading!

matrix_percent = confusion_matrix(y_test, y_pred) / len(y_test) * 100

sns.heatmap(matrix_percent,annot=True,cbar=True)
plt.ylabel('True Label')
plt.xlabel('Predicted Label')
plt.title('Confusion Matrix with %')
plt.show()

#%% The functions here are of the authorship of:
#  Wagner Cipriano, GitHub: wcipriano, Gmail: wagnerbhbr@gmail.com 
#  Under the Apache 2.0 License
#  Source: https://github.com/wcipriano/pretty-print-confusion-matrix/blob/master/confusion_matrix_pretty_print.py

#My minor changes:
"""
- show_null_values parameter wasn't working, had to comment one line 
that was setting it to 2. Now, on null cells:   0- no value
                                                1- a 0
                                                2- a 0 and 0.0% 

- commented plt.show() to be able to savefigure

- changed the names of some imports to be compactible with mine, therefore, for
this code to work, the complete import list in this file is required. Check
the link for a smaller list but don't forget to do the first modification.
"""


import matplotlib.font_manager as fm
from matplotlib.collections import QuadMesh

def get_new_fig(fn, figsize=[9,9]):
    """ Init graphics """
    fig1 = plt.figure(fn, figsize)
    ax1 = fig1.gca()   #Get Current Axis
    ax1.cla() # clear existing plot
    return fig1, ax1
#

def configcell_text_and_colors(array_df, lin, col, oText, facecolors, posi, fz, fmt, show_null_values=0):
    """
      config cell text and colors
      and return text elements to add and to dell
      @TODO: use fmt
    """
    text_add = []; text_del = [];
    cell_val = array_df[lin][col]
    tot_all = array_df[-1][-1]
    per = (float(cell_val) / tot_all) * 100
    curr_column = array_df[:,col]
    ccl = len(curr_column)

    #last line  and/or last column
    if(col == (ccl - 1)) or (lin == (ccl - 1)):
        #tots and percents
        if(cell_val != 0):
            if(col == ccl - 1) and (lin == ccl - 1):
                tot_rig = 0
                for i in range(array_df.shape[0] - 1):
                    tot_rig += array_df[i][i]
                per_ok = (float(tot_rig) / cell_val) * 100
            elif(col == ccl - 1):
                tot_rig = array_df[lin][lin]
                per_ok = (float(tot_rig) / cell_val) * 100
            elif(lin == ccl - 1):
                tot_rig = array_df[col][col]
                per_ok = (float(tot_rig) / cell_val) * 100
            per_err = 100 - per_ok
        else:
            per_ok = per_err = 0

        per_ok_s = ['%.2f%%'%(per_ok), '100%'] [per_ok == 100]

        #text to DEL
        text_del.append(oText)

        #text to ADD
        font_prop = fm.FontProperties(weight='bold', size=fz)
        text_kwargs = dict(color='w', ha="center", va="center", gid='sum', fontproperties=font_prop)
        lis_txt = ['%d'%(cell_val), per_ok_s, '%.2f%%'%(per_err)]
        lis_kwa = [text_kwargs]
        dic = text_kwargs.copy(); dic['color'] = 'g'; lis_kwa.append(dic);
        dic = text_kwargs.copy(); dic['color'] = 'r'; lis_kwa.append(dic);
        lis_pos = [(oText._x, oText._y-0.3), (oText._x, oText._y), (oText._x, oText._y+0.3)]
        for i in range(len(lis_txt)):
            newText = dict(x=lis_pos[i][0], y=lis_pos[i][1], text=lis_txt[i], kw=lis_kwa[i])
            #print 'lin: %s, col: %s, newText: %s' %(lin, col, newText)
            text_add.append(newText)
        #print '\n'

        #set background color for sum cells (last line and last column)
        carr = [0.27, 0.30, 0.27, 1.0]
        if(col == ccl - 1) and (lin == ccl - 1):
            carr = [0.17, 0.20, 0.17, 1.0]
        facecolors[posi] = carr

    else:
        if(per > 0):
            txt = '%s\n%.2f%%' %(cell_val, per)
        else:
            if(show_null_values == 0):
                txt = ''
            elif(show_null_values == 1):
                txt = '0'
            else:
                txt = '0\n0.0%'
        oText.set_text(txt)

        #main diagonal
        if(col == lin):
            #set color of the textin the diagonal to white
            oText.set_color('w')
            # set background color in the diagonal to blue
            facecolors[posi] = [0.35, 0.8, 0.55, 1.0]
        else:
            oText.set_color('r')

    return text_add, text_del
#

def insert_totals(df_cm):
    """ insert total column and line (the last ones) """
    sum_col = []
    for c in df_cm.columns:
        sum_col.append( df_cm[c].sum() )
    sum_lin = []
    for item_line in df_cm.iterrows():
        sum_lin.append( item_line[1].sum() )
    df_cm['sum_lin'] = sum_lin
    sum_col.append(np.sum(sum_lin))
    df_cm.loc['sum_col'] = sum_col
    #print ('\ndf_cm:\n', df_cm, '\n\b\n')
#
def pretty_plot_confusion_matrix(df_cm, annot=True, cmap="Oranges", fmt='.2f', fz=11,
      lw=0.5, cbar=False, figsize=[8,8], show_null_values=0, pred_val_axis='y'):
    """
      print conf matrix with default layout (like matlab)
      params:
        df_cm          dataframe (pandas) without totals
        annot          print text in each cell
        cmap           Oranges,Oranges_r,YlGnBu,Blues,RdBu, ... see:
        fz             fontsize
        lw             linewidth
        pred_val_axis  where to show the prediction values (x or y axis)
                        'col' or 'x': show predicted values in columns (x axis) instead lines
                        'lin' or 'y': show predicted values in lines   (y axis)
    """
    if(pred_val_axis in ('col', 'x')):
        xlbl = 'Predicted'
        ylbl = 'Actual'
    else:
        xlbl = 'Actual'
        ylbl = 'Predicted'
        df_cm = df_cm.T

    # create "Total" column
    insert_totals(df_cm)

    #this is for print allways in the same window
    fig, ax1 = get_new_fig('Conf matrix default', figsize)

    #thanks for seaborn
    ax = sns.heatmap(df_cm, annot=annot, annot_kws={"size": fz}, linewidths=lw, ax=ax1,
                    cbar=cbar, cmap=cmap, linecolor='w', fmt=fmt)

    #set ticklabels rotation
    ax.set_xticklabels(ax.get_xticklabels(), rotation = 45, fontsize = 10)
    ax.set_yticklabels(ax.get_yticklabels(), rotation = 25, fontsize = 10)

    # Turn off all the ticks
    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False

    #face colors list
    quadmesh = ax.findobj(QuadMesh)[0]
    facecolors = quadmesh.get_facecolors()

    #iter in text elements
    array_df = np.array( df_cm.to_records(index=False).tolist() )
    text_add = []; text_del = [];
    posi = -1 #from left to right, bottom to top.
    for t in ax.collections[0].axes.texts: #ax.texts:
        pos = np.array( t.get_position()) - [0.5,0.5]
        lin = int(pos[1]); col = int(pos[0]);
        posi += 1
        #print ('>>> pos: %s, posi: %s, val: %s, txt: %s' %(pos, posi, array_df[lin][col], t.get_text()))

        #set text
        txt_res = configcell_text_and_colors(array_df, lin, col, t, facecolors, posi, fz, fmt, show_null_values)

        text_add.extend(txt_res[0])
        text_del.extend(txt_res[1])

    #remove the old ones
    for item in text_del:
        item.remove()
    #append the new ones
    for item in text_add:
        ax.text(item['x'], item['y'], item['text'], **item['kw'])

    #titles and legends
    ax.set_title('Confusion matrix')
    ax.set_xlabel(xlbl)
    ax.set_ylabel(ylbl)
    plt.tight_layout()  #set layout slim
    #plt.show()
#

def plot_confusion_matrix_from_data(y_test, predictions, columns=None, annot=True, cmap="Oranges",
      fmt='.2f', fz=11, lw=0.5, cbar=False, figsize=[8,8], show_null_values=0, pred_val_axis='lin'):
    """
        plot confusion matrix function with y_test (actual values) and predictions (predic),
        whitout a confusion matrix yet
    """
    from sklearn.metrics import confusion_matrix
    from pandas import DataFrame

    #data
    if(not columns):
        #labels axis integer:
        ##columns = range(1, len(np.unique(y_test))+1)
        #labels axis string:
        from string import ascii_uppercase
        columns = ['class %s' %(i) for i in list(ascii_uppercase)[0:len(np.unique(y_test))]]

    confm = confusion_matrix(y_test, predictions)
    cmap = 'Oranges';
    fz = 11;
    figsize=[9,9];
    #show_null_values = 0
    df_cm = DataFrame(confm, index=columns, columns=columns)
    pretty_plot_confusion_matrix(df_cm, fz=fz, cmap=cmap, figsize=figsize, show_null_values=show_null_values, pred_val_axis=pred_val_axis)
#

#%%

classes = [str(n) for n in range(10)]
plot_confusion_matrix_from_data(y_test, y_pred, columns=classes, show_null_values=2)

#Sh*t... the axis are the other way around! Well, I can transpose...
sns.heatmap(np.transpose(matrix),annot=True,cbar=True)
plt.xlabel('True Label') #and switch the axis labels
plt.ylabel('Predicted Label')
plt.title('Confusion Matrix with %')
plt.show()

#%% Without the stopper


#%% First Model
model = keras.models.Sequential()

model.add(keras.layers.Flatten(input_shape = (28,28,1)))
model.add(keras.layers.Dense(64, activation='relu'))
model.add(keras.layers.Dense(128, activation='relu'))
model.add(keras.layers.Dense(10, activation='softmax'))

model.summary()

Adam = keras.optimizers.Adam(lr=0.01, clipnorm = 1)

model.compile(loss='categorical_crossentropy', optimizer=Adam)

hist2 = \
    model.fit(x = x_norm, y=labels, batch_size=300, epochs=400,\
        verbose = 0, callbacks = None, validation_split=0.3)

y_pred_labels2 = model.predict(x_test_norm)

y_pred2 = np.argmax(y_pred_labels2, axis=1)

#%%
plt.plot(hist2.history['loss'])
plt.plot(hist2.history['val_loss'])
plt.title('Model loss without EarlyStopping')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('lossVSvalNOearlystop.png')
plt.show()


#%%


matrix = confusion_matrix(y_test, y_pred)

print('Accuracy With Stopper:', accuracy_score(y_test, y_pred, normalize=True)* 100, '%')


matrix2 = confusion_matrix(y_test, y_pred2)

print('Accuracy Without Stopper:', accuracy_score(y_test, y_pred2, normalize=True)* 100, '%')

#%%
plt.figure()
plot_confusion_matrix_from_data(y_test, y_pred2, columns=classes, show_null_values=0)
plt.savefig('hel.png')
plt.show()

#%%

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15,5))
fig.suptitle('Confusion Matrices comparison')

sns.heatmap(matrix,annot=True,cbar=False, ax = ax1) #ax1.plot(x, y)
ax2.set(xlabel='True Label', ylabel='Predicted Label')
ax1.set_title('With EarlyStopping')


sns.heatmap(matrix2,annot=True,cbar=False, ax = ax2)
ax2.set_title('Without EarlyStopping')
ax2.set(xlabel='True Label', ylabel='Predicted Label')

fig.savefig('Comp.png')#, bbox_inches='tight')

#%% Another way

f = plt.figure(figsize=(10,3))

ax1 = plt.subplot(1,2,1)
sns.heatmap(matrix,annot=True,cbar=False, ax = ax1)

ax2 = plt.subplot(1,2,2)
sns.heatmap(matrix2,annot=True,cbar=False, ax = ax2)
plt.title("helo")

#%%

# String axis!! For Classes like 'A', 'B', etc... may be useful:
# https://stackoverflow.com/questions/5821125/how-to-plot-confusion-matrix-with-string-axis-rather-than-integer-in-python


#%% CNN



from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense

model2 = keras.models.Sequential()

#A flatten layer to sequentialize the input: height x width x depth
# output will be: (row after row... 1D with 28x28x1 length)
#input shape of one sample
#in case the samples are one dimensional, Input layer is enough.

#after the first layer it is not necessary to specify the input size
model2.add(Conv2D(16,kernel_size=(3,3), activation='relu'))
model2.add(MaxPooling2D(pool_size=(2,2)))

model2.add(Conv2D(32,kernel_size=(3,3), activation='relu'))
model2.add(MaxPooling2D(pool_size=(2,2)))


model2.add(Flatten(input_shape = (5,5,32)))
model2.add(Dense(64, activation='relu'))
model2.add(Dense(10, activation='softmax'))

#keras.layers.Conv2D(filters, kernel_size, strides=(1, 1), \
# padding='valid', data_format=None, dilation_rate=(1, 1), \
# activation=None, use_bias=True, kernel_initializer='glorot_uniform', \
# bias_initializer='zeros', kernel_regularizer=None, bias_regularizer=None, \
# activity_regularizer=None, kernel_constraint=None, bias_constraint=None)


model2.compile(loss='categorical_crossentropy', optimizer=Adam)
#same EarlyStopper, optimizer and lossfunction.
histCNN = \
    model2.fit(x = x_norm, y=labels, batch_size=300, epochs=400,\
        verbose = 0, callbacks = [stopper], validation_split=0.3)
#shuffle??
#whether to shuffle the training data before each epoch

model2.summary()

#%%
f = plt.figure(figsize=(10,3))

ax1 = plt.subplot(1,2,2)
plt.plot(histCNN.history['loss'])
plt.plot(histCNN.history['val_loss'])
plt.title('CNN Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'])


ax2 = plt.subplot(1,2,1)
plt.plot(hist1.history['loss'])
plt.plot(hist1.history['val_loss'])
plt.title('MLP Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'])

plt.show()

f.savefig('LossComp.png', bbox_inches='tight')



#%%

y_pred_labels3 = model2.predict(x_test_norm) #model2 is the correct one...

y_pred3 = np.argmax(y_pred_labels3, axis=1)

print('CNN Accuracy:', accuracy_score(y_test, y_pred3, normalize=True)* 100, '%')

matrix3 = confusion_matrix(y_test, y_pred3)


#%%
matplotlib.style.use('default')
f = plt.figure(figsize=(10,3))

ax1 = plt.subplot(1,2,1)
sns.heatmap(matrix,annot=True,cbar=False, ax = ax1)
plt.title("Normal 3-Dense NN")
plt.ylabel('True Label')
plt.xlabel('Predicted Label')

ax2 = plt.subplot(1,2,2)
sns.heatmap(matrix3,annot=True,cbar=False, ax = ax2)
plt.title("Convolutional NN")
plt.ylabel('True Label')
plt.xlabel('Predicted Label')


f.savefig('MPLvsCNN2.png', bbox_inches = 'tight')










